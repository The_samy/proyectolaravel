<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
                'id'=> 2,
                'name'=> 'admina',
                'email'=>'admina@localhost.test',
                'password'=> bcrypt('admin'),
                'email_verified_at' => now(),
                'remember_token' => Str::random(10),

        ]);

        $users = factory(App\User::class,10)->create();
    }

}
