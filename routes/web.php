<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/manage/dashboard', function (){

    return view('manage.dashboard');
})->name('manage.dashboard');

Route::get('/manage', function (){
    return view('manage.dashboard');
})->name('manage');


Route::resource('/manage/users','UsersController');
Route::resource('/manage/roles','RolesController');
Route::resource('/manage/permisos','PermissionsController');

