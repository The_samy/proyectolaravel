<?php

namespace App\Http\Controllers;

use App\Models\Permission;
use Illuminate\Http\Request;

class PermissionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions = Permission::active()->get();
        $model_text =  trans('models.permission') ;
        $model = 'permission';

        return view('manage.permisos.index',compact('permissions', 'model' , 'model_text'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("manage.permisos.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $permission = new permission($this->validateFields($request));

        $permission->save();

        $model = trans('models.permission');

        $request->session()->flash("flash_message",trans('messages.success_created',['model' =>  $model ]));

        return redirect('/manage/permisos');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function show(Permission $permission)
    {
        return view('manage.permisos.vista', compact('permission'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function edit(Permission $permission)
    {
        return view('manage.permisos.edit',compact('permission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Permission $permission)
    {
        $permission ->update($this->validateFields($request));

        $permission->save();

        $model = trans('models.permission');

        $request->session()->flash("flash_message",trans('messages.success_updated',['model' =>  $model ]));

        return redirect('/manage/permisos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function destroy(Permission $permission)
    {
        $permission->active = 0;

        $permission->save();

        $model = trans('models.permission');

        request()->session()->flash("flash_message",trans('messages.success_deleted',['model' =>  $model ]));

        return redirect('/manage/permisos');
    }

    public function validateFields(Request $request){

        $validatedData = $request->validate(
            [
                'name' => 'required',
                'description' => 'required'
            ],
            [
                'name.required' => trans('validation.filled', ['attribute' => 'nombre']),
                'description.required' => trans('validation.filled', ['attribute' => 'descripción'])
            ]
        );

        return $validatedData;
    }
}
