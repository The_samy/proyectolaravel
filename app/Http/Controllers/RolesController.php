<?php

namespace App\Http\Controllers;

use App\Models\Role;
use Illuminate\Http\Request;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::active()->get();
        $model_text =  trans('models.role') ;
        $model = 'role';

        return view('manage.roles.index',compact('roles' ,'model_text', 'model'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('manage.roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $role = new Role($this->validateFields($request));

        $role->save();

        $model = trans('models.role');

        $request->session()->flash("flash_message",trans('messages.success_created',['model' =>  $model ]));

        return redirect('/manage/roles');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        return view('manage.roles.vista',compact('role'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        return view('manage.roles.edit',compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        $role ->update($this->validateFields($request));

        $role->save();

        $model = trans('models.role');

        $request->session()->flash("flash_message",trans('messages.success_updated',['model' =>  $model ]));

        return redirect('/manage/roles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        $role->active = 0;

        $role->save();

        $model = trans('models.role');

        request()->session()->flash("flash_message",trans('messages.success_deleted',['model' =>  $model ]));

        return redirect('/manage/roles');
    }


    public function validateFields(Request $request){

        $validatedData = $request->validate(
            [
                'name' => 'required',
                'description' => 'required',
              
            ],
            [
                'name.required' => 'Por favor introduzca un nombre.',
                'description.required' =>'debe colocar una descripcion'
               
            ]
        );

        return $validatedData;
    }


}
