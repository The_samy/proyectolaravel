@extends('layouts.admin')

@section('main_content')
   

    <!-- Main content -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <!-- /.card-header -->
             
                <div class="card-body">
                    <table id="users_table" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Descripcion</th>
                         
                        </tr>
                        </thead>

                        <tbody>
                       
                            <tr>
                                <td>{{ $rol->id }}</td>
                                <td>{{ $rol->name }}</td>
                                <td>{{ $rol->description}} </td>
                               
                                
                            </tr>
                   


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script>
    document.addEventListener("DOMContentLoaded", function () {

        $('#users_table').DataTable({
            "responsive": true,
            "autoWidth": false,
        });

    });
    </script>

@endsection
