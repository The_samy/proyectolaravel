@extends('layouts.admin')


@section('main_content')
 
 <h1>Nuevo Rol</h1>
 <!-- Main content -->
  <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Agregar Rol</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" name="users" method="post" action="{{ url('/manage/roles') }}" id="roles">
                <div class="card-body">
                    @csrf 
                    @method('POST')
                    @foreach($errors->all() as $error)
                    <div class="alert alert-danger">{{ $error }}</div>
                    @endforeach
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control" id="name" placeholder="Enter name"   value="{{ old('name') }}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">descriptions</label>
                    <input type="text" name="description" class="form-control" id="exampleInputEmail1" placeholder="Enter descriptions"   value="{{ old('description') }}">
                  </div>

              
               
                  <div class="form-group mb-0">
                   
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary float-right m-3">Crear</button>
                    <button type="reset" class="btn btn-default float-right m-3">Cancelar</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
    <!-- /.container-fluid -->
    

</div>
  
@endsection