@extends('layouts.admin')

@section('main_content')
    <div class="container d-flex justify-content-between">
        <div>
            <h1>{{$model_text}}s</h1>
        </div>
        <div class="d-flex align-items-center">
            <a href="{{ action(  ucfirst($model) . 's' . 'Controller@create') }}" class="btn btn-block btn-primary">Crear {{ strtolower( $model_text ) }}</a>
        </div>
    </div>

    <!-- Main content -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <!-- /.card-header -->
                @if(session()->has('flash_message'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>Excelente!</strong> {{ session()->get('flash_message') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                <div class="card-body">
                    <table id="users_table" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Descripcion</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach ($permissions as $per)
                            <tr>
                                <td>{{ $per->id }}</td>
                                <td>{{ $per->name }}</td>
                                <td>{{ $per->description}} </td>
                                <td>
                                    <div class="d-flex">
                                        <ul class="list-inline center mx-auto justify-content-center m-0">
                                            <li class="list-inline-item">
                                                <a class="nav-link" href="{{ url('/manage/permisos/' . $per->id. '/vista' )  }}"
                                                   role="button"><i class="fas fa-book-open"></i></a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a class="nav-link"
                                                   href="{{ url('/manage/permisos/' . $per->id ) . '/edit' }}"
                                                   role="button"><i class="fas fa-edit"></i></a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a class="nav-link" href="#" role="button"
                                                   onclick="deleteModelRecord({{$per->id}})"><i
                                                        class="fas fa-trash-alt"></i></a>
                                                <pre delete-dialog-model="deleteModelRecord" class="d-none">

                                        <form id="deleteModelRecord" name="delteModelRecord"
                                              action="{{ url('/manage/permisos/')}}" method="POST">
                                            @method('DELETE')
                                            @csrf
                                        </form>


                                        <script>

                                            function deleteModelRecord (id) {

                                                url = $('#deleteModelRecord').attr('action') + "/" + id;

                                                Swal.fire({
                                                    title: '¿Estás seguro que deseas eliminar este registro?',
                                                    text: "La acción no podrá ser revertida!",
                                                    icon: 'warning',
                                                    showCancelButton: true,
                                                    confirmButtonColor: '#3085d6',
                                                    cancelButtonColor: '#d33',
                                                    confirmButtonText: 'Sí, eliminarlo!',
                                                    cancelButtonText: 'Cancelar',
                                                }).then((result) => {
                                                    if (result.value) {

                                                        $('#deleteModelRecord').attr('action', url).submit();

                                                    }
                                                });
                                            }


                                        </script>
                                        </pre>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script>
    document.addEventListener("DOMContentLoaded", function () {

        $('#users_table').DataTable({
            "responsive": true,
            "autoWidth": false,
        });

    });
    </script>

@endsection
