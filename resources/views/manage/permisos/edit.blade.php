@extends('layouts.admin')


@section('main_content')

<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- jquery validation -->

        <div class="card card-primary mt-4 ">
            <div class="card-header mb-4">
                <h3 class="card-title">Actualizar Permiso</h3>
            </div>
           
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" id="users" name="users" method="post" action="{{ url('/manage/permisos/' . $permission->id) }}">
                <div class="card-body">
                    @method('PATCH')
                    @csrf

                    @include('manage.permisos.fields')
                    
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary float-right m-3">Guardar</button>
                    <button type="button" onclick="window.history.back()" class="btn btn-default float-right m-3">Cancelar</button>
                </div>
            </form>
        </div>
        <!-- /.card -->
    </div>
    <!--/.col (left) -->
    <!-- right column -->
    <div class="col-md-6">

    </div>
    <!--/.col (right) -->
</div>
<!-- /.row -->


@endsection