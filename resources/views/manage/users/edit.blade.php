@extends('layouts.admin')


@section('main_content')

<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- jquery validation -->

        <div class="card card-primary mt-4 ">
            <div class="card-header mb-4">
                <h3 class="card-title">Actualizar usuario</h3>
            </div>
           
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" id="users" name="users" method="post" action="{{ url('/manage/users/' . $user->id) }}">
                <div class="card-body">
                    @method('PATCH')
                    @csrf

                    @include('manage.users.fields')
                    
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary float-right m-3">Guardar</button>
                    <button type="button" onclick="window.history.back()" class="btn btn-default float-right m-3">Cancelar</button>
                </div>
            </form>
        </div>
        <!-- /.card -->
    </div>
    <!--/.col (left) -->
    <!-- right column -->
    <div class="col-md-6">

    </div>
    <!--/.col (right) -->
</div>
<!-- /.row -->

<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function() {
      $.validator.setDefaults({
        submitHandler: function () {
          $("#users").submit();
        }
      });
      $('#users').validate({
        rules: {
          name: {
            required: true
          },
          email: {
            required: true,
            email: true,
          },
          password: {
            required: true,
            minlength: 8,
            maxlength:45
          },
          password1: {
            required: true,
            equalTo: "#password"
          },

        },
        messages: {
          name: {
            required: "Por favor introduzca un nombre."
          },
          email: {
            required: "Por favor introduzca su correo electrónico.",
            email: "Por favor introduzca un correo electrónico válido."
          },
          password: {
            required: "Por favor introduzca una contraseña",
            minlength: "Su contraseña debe de tener al menos 8 caracteres.",
            maxlength: "Su contraseña no debe contener mas de 45 caracteres."
          },
          password1: {
            required: "Por favor debe repetir la contraseña.",
            equalTo: "Las contraseñas introducidas no coinciden."
          },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        }
      });
    });
</script>

@endsection