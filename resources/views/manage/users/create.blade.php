@extends('layouts.admin')


@section('main_content')
 
 <h1>Nuevo usuario</h1>
 <!-- Main content -->
  <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Agregar usuario</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" name="users" method="post" action="{{ url('/manage/users') }}" id="users">
                <div class="card-body">
                    @csrf 
                    @method('POST')
                    @foreach($errors->all() as $error)
                    <div class="alert alert-danger">{{ $error }}</div>
                    @endforeach
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control" id="name" placeholder="Enter name"   value="{{ old('name') }}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email"   value="{{ old('email') }}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword2">Confirm password</label>
                    <input type="password" name="password2" class="form-control" id="exampleInputPassword2" placeholder="Password2">
                  </div>
                  <div class="form-group mb-0">
                   
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary float-right m-3">Crear</button>
                    <button type="reset" class="btn btn-default float-right m-3">Cancelar</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
    <!-- /.container-fluid -->
    
<script type="text/javascript">
  document.addEventListener("DOMContentLoaded", function(){
    $.validator.setDefaults({
        submitHandler: function () {
            $("#users").submit();
        }
      });
  /*$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });*/
  $('#users').validate({
    rules: {
        name:{
                required: true
        },
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      password2: {
        required: true,
        equalTo: "#password"
      },
    },
    messages: {
        name:{
                required: "el nombre es requerido"
        },
      email: {
        required: "el email es requerido",
        email: "debe colocar un email valido"
      },
      password: {
        required: "el password es requerido",
        minlength: "la contraseñs debe tener un minimo de 5 caracteres"
      },
      password2: {
        required: "debe repetir la contraseña",
        equalTo: "las contraseñas no coinciden"
      },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
    });
  
});
</script>
</div>
  
@endsection